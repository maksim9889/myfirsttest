var dialogWithUser;


window.onload = function()
{
	dialogWithUser = document.getElementById("dialogWithUser");
	
	var submitBtn = document.getElementById("submitBtn");
	
	//// onclick events
	submitBtn.onclick=function(e)
	{
		e.preventDefault();
		var userName = document.getElementById("userName").value;
		var password = document.getElementById("password").value;
		var dbUrl = "includes/call.php";
		var strData = "userName="+userName+"&password="+password;
		sendPostRequest(dbUrl,strData);
	}
	
}



//post request for database saving
function sendPostRequest(dbUrl, strData)
{
	var request = new XMLHttpRequest();
	request.open("POST",dbUrl,true);
	request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	request.setRequestHeader("Content-length", strData.length);
	request.setRequestHeader("Connection", "close");
	request.onload = function()
	{
		if(request.status==200)
		{
			if(request.responseText.indexOf("Welcome")!=-1)
			{
				var myLoginForm = document.getElementById("myLoginForm");
				if(myLoginForm)
					myLoginForm.style.display = "none";
					
			}
			
			dialogWithUser.innerHTML = request.responseText;
			
		}
	}
	request.send(strData);
}

///////////////////parsing (not used yet)
function parseJSON(stringJSON)
{
	var result = JSON.parse(stringJSON);
	//console.log(result);
	return result;
}

