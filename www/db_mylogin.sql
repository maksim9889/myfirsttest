-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 02, 2013 at 08:52 AM
-- Server version: 5.5.25
-- PHP Version: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: 'db_mylogin'
--

-- --------------------------------------------------------

--
-- Table structure for table 'tbl_users'
--

CREATE TABLE tbl_users (
  id smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  username varchar(15) NOT NULL,
  `password` varchar(32) NOT NULL,
  firstname varchar(15) NOT NULL,
  lastname varchar(15) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1;

--
-- Dumping data for table 'tbl_users'
--

INSERT INTO tbl_users (id, username, `password`, firstname, lastname) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Maks', 'Mykhailov');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
