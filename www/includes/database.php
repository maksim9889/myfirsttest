<?php
require_once("config.php");

class Database
{
	
	//////////////////properties
	
	//public properties
	public $last_query;
	
	//private properties
	private $connection;
	private $magic_quotes_active;
	private $real_escape_string_exists;
	
	
	/////////////////public methods
	
	//constructor method
	function __construct()
	{
		$this->open_connection();
		session_start();
	}
	
	//opens database connection
	public function open_connection()
	{
		$this->connection = mysql_connect(DB_SERVER, DB_USER, DB_PASS);
		if(!$this->connection)
		{
			die("Database connection failed:". mysql_error());
		}
		else
		{
			$db_select = mysql_select_db("db_mylogin", $this->connection);
			if(!$db_select)
			{
				die("Database selection failed:".mysql_error());
			}
			
		}
	}
	
	//closes database connection
	public function close_connection()
	{
		if(isset($this->connection))
		{
			mysql_close($this->connection);
			unset($this->connection);
		}
	}
	
	//performs the query on the database
	public function query($sql)
	{
		$this->last_query = $sql;
		$result = mysql_query($sql);
		$this->confirm_query($result);
		return $result;
	}
	
	public function get_last_id()
	{
		$result = mysql_insert_id();
		return $result;
	}
	//escapes everything for the database                          
	public function escape_value($value)
	{
		if($this->real_escape_string_exists)
		{
			if($this->magic_quotes_active)
			{
				$value = stripslashes($value);
			}
			
			$value = mysql_real_escape_string($value);
		}
		
		else if(!$this->magic_quotes_active)
		{
			$value = addslashes($value );
		}
		return $value;
	}
	
	//getting ready for JSON format
	public function prepare_JSON($str)
	{
		$str = stripslashes($str);
		$str = addcslashes($str, '"');
		return $str;
	}
	
	//clear string output: removing spaces and last character
	public function strip_output($str)
	{
		$str = trim($str);
		$length = strlen($str);
		$str = substr($str, 0, $length-1); 
		return $str;
	}
	/////////////////private methods
	
	//confirms the result of query execution
	private function confirm_query($result)
	{
		if(!$result)
		{
			die("Database query failed:".mysql_error());
		}
	}
	
}

$db = new Database();

?>
