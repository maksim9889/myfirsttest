<?php
require_once("database.php");

class User
{
	public $username;
	public $password;
	
	private static function instantiate($record) 
	{
    	$object = new self;
		$object->username = $record['username'];
		$object->password = $record['password'];
		//$object->first_name = $record['first_name'];
		//$object->last_name 	= $record['last_name'];
		return $object;
	}
	
	//apply encryption to the password
	private static function protect_password($password)
	{
		$password=trim($password);
		$password=md5($password);
		return $password;
	}
	
	//checks up if the particular user exists
	private static function authenticate($username="", $password="") 
	{
		global $db;
		//some preparation
		$username = trim($username);
		$username = $db->escape_value($username);
		$password = $db->escape_value($password);
		$password = self::protect_password($password);
		
		$sql  = "SELECT * FROM tbl_users ";
		$sql .= "WHERE username = '{$username}' ";
		$sql .= "AND password = '{$password}' ";
		$sql .= "LIMIT 2";
		$result_array = self::find_by_sql($sql);
		
		
		//return !empty($result_array) ? array_shift($result_array) : false;
		if(empty($result_array))
			return NULL;
			
		if(count($result_array) > 1)
		{
			//handle the error (more than one of the same user)
		 	// TODO: write some
		}
		return array_shift($result_array);
	}
	
	public static function echo_authenticate_result($username="", $password="")
	{
		if($username == "")
		{
			echo "Username is empty";
			return;
		}
			
		if($password == "")
		{
			echo "Password is empty";	
			return;
		}
			
		$rez = self::authenticate($username, $password);
		if($rez == NULL)
		{
			echo "Username or password is wrong";	
			return;
		}
		
		echo "Welcome ".$username.", you successfully loged in!";
	}
 
 	//find a row by sql
 	private static function find_by_sql($sql="") 
	{
    	global $db;
    	$result_set = $db->query($sql);
    	$object_array = array();
		
    	while ($row = mysql_fetch_array($result_set)) 
		{
			$object_array[] = self::instantiate($row);
    	}
    	return $object_array;
  	}
	
}

